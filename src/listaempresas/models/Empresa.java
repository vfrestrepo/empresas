/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listaempresas.models;

/**
 *
 * @author Some
 */
public class Empresa {

    private String nit;
    private String nombre;
    private String regimen;
    private String tipoSociedad;
    private String gerente;

    public Empresa(String nit, String nombre, String regimen, String tipoSociedad, String gerente) {
        this.nit = nit;
        this.nombre = nombre;
        this.regimen = regimen;
        this.tipoSociedad = tipoSociedad;
        this.gerente = gerente;
    }

    public void getTSociedad(String radio) {
        tipoSociedad = radio;
    }

    public String setTSociedad() {
        return tipoSociedad;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the regimen
     */
    public String getRegimen() {
        return regimen;
    }

    /**
     * @param regimen the regimen to set
     */
    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    /**
     * @return the tipoSociedad
     */
    public String getTipoSociedad() {
        return tipoSociedad;
    }

    /**
     * @param tipoSociedad the tipoSociedad to set
     */
    public void setTipoSociedad(String tipoSociedad) {
        this.tipoSociedad = tipoSociedad;
    }

    /**
     * @return the gerente
     */
    public String getGerente() {
        return gerente;
    }

    /**
     * @param gerente the gerente to set
     */
    public void setGerente(String gerente) {
        this.gerente = gerente;
    }

    @Override
    public String toString() {
        return "Empresa: " + nombre + "\nNit " + nit + "\nRegimen " + regimen + "\nTipo de sociedad " + tipoSociedad
                + "\nGerente " + gerente;

    }

}
